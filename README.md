# OpenML dataset: xd6

https://www.openml.org/d/40693

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Unknown  
**Source**: [PMLB](https://github.com/EpistasisLab/penn-ml-benchmarks/tree/master/datasets/classification) - Supposedly originates from UCI, but can't find it there anymore.  
**Please cite:**  

**XD6 Dataset**
Dataset used by Buntine and Niblett (1992). Composed of 10 features, one of which is irrelevant. The target is a disjunctive normal form formula over the nine other attributes, with additional classification noise.

[More info](https://books.google.be/books?id=W2bmBwAAQBAJ&pg=PA313&lpg=PA313&dq=dataset+xd6&source=bl&ots=6hYPdz8_Nl&sig=TR1ieOg9D1pCrvNyeKbb-3eKmd8&hl=en&sa=X&ved=0ahUKEwj_tZ_MxozZAhVHa1AKHZVEBBsQ6AEIQjAF#v=onepage&q=dataset xd6&f=false).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/40693) of an [OpenML dataset](https://www.openml.org/d/40693). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/40693/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/40693/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/40693/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

